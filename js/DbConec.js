import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyDkwZiz02ieyMQACtTNK5-pZHoej00l9Cw",
    authDomain: "bd-third-word-toy-s.firebaseapp.com",
    databaseURL: "https://bd-third-word-toy-s-default-rtdb.firebaseio.com",
    projectId: "bd-third-word-toy-s",
    storageBucket: "bd-third-word-toy-s.appspot.com",
    messagingSenderId: "1048182829696",
    appId: "1:1048182829696:web:ff556ccb134b8c55d7ac65"
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);