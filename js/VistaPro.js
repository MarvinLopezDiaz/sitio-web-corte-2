import {auth} 
from "./DbConec.js";
import {getDatabase, ref, onValue }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js"

var ProductosNav = document.getElementById('Productos');

imprimirProductos();

function imprimirProductos(){
    const db = getDatabase();
    const dbRef = ref(db, 'Productos');
    onValue(dbRef, (snapshot)=>{
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if(childData.Estado == 0){
                ProductosNav.innerHTML +=
                    "<picture>" +
                        "<img id='imgPro' src='" + childData.url + "' alt='pro1'>  " +
                        "<p id='pPro'>" +
                            childData.Nombre +
                            "<br> $" +
                            childData.Precio +
                            "<br>" +
                            childData.Descripcion +
                        "</p>" +
                        "<button id='botonesProductos'>Comprar</button>" +
                    "</picture>";
            }
        });
    },{
        onlyOnce: true
    });
}