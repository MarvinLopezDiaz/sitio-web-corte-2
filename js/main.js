import {getDatabase,ref,set,child,get,update,remove, onValue }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js"
import { getStorage, ref as refS, uploadBytes,getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
import {auth} 
from "./DbConec.js";
import { onAuthStateChanged, signOut } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"

const db = getDatabase();


const btnCerrar = document.getElementById('cerrarSec');
//botones
var btnAgregar = document.getElementById('agregar');
var btnBuscar = document.getElementById('buscar');
var btnBuscarID = document.getElementById('buscarID');
var btnModificar = document.getElementById('modificar');
var btnImagen = document.getElementById('imagBoton');
var btnLimpiar = document.getElementById('limpiar');

//Inputs
var IDTxT = document.getElementById('ID');
var ImagenPro = document.getElementById('imgPro');
var ImagenSu = document.getElementById('EsIma');
var NombreIma = document.getElementById('nombreIma');
var EstadoTxT = document.getElementById('estadoTxT');
var EstadoT = document.getElementById('estado');
var ID = document.getElementById('ID').value = Math.floor(Math.random()*10000);

// Validaciones inputs
var Nombre = "";
var Precio = 0.0;
var Descripcion = "";
var Estado = 0;
var archivo = "";
var nomImgLocal = "";
var nomImgConsu = "";
var urlIma = "";

onAuthStateChanged(auth,(user)=>{
    if(user){
        alert('Bienvenido Usuario');
    }else{
        location = '/html/Login.html';
        alert('No iniciaste secion');
    }
});

btnCerrar.addEventListener('click', async()=>{
    await signOut(auth);
    alert('Cerrando secion');
    location = '/html/Login.html';
});

function Limpiar(){
    Nombre = document.getElementById('nombre').value = '';
    document.getElementById("nombre").disabled = false;
    Precio = document.getElementById('precio').value = '';
    Descripcion = document.getElementById('descripcion').value = '';
    NombreIma.value = '';
    ImagenSu = document.getElementById('EsIma').value = '';
    archivo = "";
    nomImgLocal = "";
    nomImgConsu = "";
    ImagenPro.src = "/img/ProductoIcono.png"
    document.getElementById('agregar').style.display = 'block';
    EstadoTxT.style.display = "none";
    Estado = document.getElementById('estado').value = '';
    EstadoT.style.display = "none";
    document.getElementById('buscarID').style.display = 'none';
    document.getElementById('buscar').style.display = 'block';
    document.getElementById("buscarID").disabled = false;
    document.getElementById("buscar").disabled = false;
    document.getElementById('limpiar').style.display = 'block';
}

function camposLlenos(){
    let exito = true;

    if(Nombre.value == ""
    || Descripcion.value == ""
    || Precio.value == ""){
        exito = false;
    }

    return exito;
}

function LeerInputs(){
    Nombre = document.getElementById('nombre').value;
    Precio = document.getElementById('precio').value;
    Descripcion = document.getElementById('descripcion').value;
    Estado = document.getElementById('estado').value;
}

function cargarImagen(){
    // Obtener imagen
    archivo = event.target.files[0];
    nomImgLocal = event.target.files[0].name;

    // Mostrar nombre
    NombreIma.value = nomImgLocal;
    document.getElementById('EsIma').style.display = 'none';
    document.getElementById('agregar').style.display = 'block';
    document.getElementById('buscar').style.display = 'block';
    document.getElementById('modificar').style.display = 'block';
    document.getElementById('imagBoton').style.display = 'block';
    document.getElementById('cerrarSec').style.display = 'block';
    document.getElementById('limpiar').style.display = 'block';
}

async function subirImagen(){
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + nomImgLocal);
    let exito;
    await uploadBytes(storageRef, archivo).then((snapshot) => {
        alert("Subida con exito");
        exito = true;
    })
    .catch((error) => {
        exito = false;
    });

    return exito;
}

async function bajarImagen(){
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + nomImgLocal);
    let exito;
    await getDownloadURL(storageRef).then((url) => {
        urlIma = url;
        exito = true; 
    })
    .catch((error) => {
        exito = false;
    });

    return exito;
}

function inserDataBase(){
    LeerInputs();
    ID = document.getElementById('ID').value = Math.floor(Math.random()*10000);
    set(ref(db,'Productos/' + ID),{
        Nombre:Nombre,
        Precio:Precio, 
        Descripcion:Descripcion,
        Estado:0,
        url:urlIma,
        NombreIma:nomImgLocal}).then((res)=>{
            ImagenPro.src = urlIma;
            alert("Subido Con exito");
    }).catch((error)=>{
        ImagenPro.src = "/img/ProductoIcono.png"
        alert("Surgio un error ");
    });
}


function MostarDatosBase(){
    ID = IDTxT.value;
    Limpiar();
    const dbref = ref(db);
    if(IDTxT.value == ""){
        alert('Falta ingresar el nombre');
    }else{
        EstadoTxT.style.display = 'block';
        EstadoT.style.display = 'block';
        get(child(dbref,'Productos/' + ID)).then((snapshot) => {
        
            if(!snapshot.exists()){
                alert('Ese producto no existe');
            }

            if(snapshot.val().Estado == "0"){
                Nombre = snapshot.val().Nombre;
                Precio = snapshot.val().Precio;
                Descripcion = snapshot.val().Descripcion;
                ImagenPro.src = snapshot.val().url;
                urlIma = snapshot.val().url;
                nomImgLocal = snapshot.val().NombreIma;
                nomImgConsu = snapshot.val().NombreIma;
                Estado = snapshot.val().Estado;
                esribirInputs();
            } else if(snapshot.val().Estado == "1"){
                alert('Ese producto esta agotado');
                console.log('Entro');
                Nombre = snapshot.val().Nombre;
                Precio = snapshot.val().Precio;
                Descripcion = snapshot.val().Descripcion;
                ImagenPro.src = snapshot.val().url;
                urlIma = snapshot.val().url;
                nomImgLocal = snapshot.val().NombreIma;
                nomImgConsu = snapshot.val().NombreIma;
                Estado = snapshot.val().Estado;
                esribirInputs();
            }
        }).catch((error)=>{
            alert("Surgio un error" + error)
        });
        document.getElementById('agregar').style.display = 'none';
        document.getElementById("ID").disabled = true;
        
        document.getElementById('buscar').style.display = 'block';
        document.getElementById('buscarID').style.display = 'none';
        document.getElementById('modificar').style.display = 'block';
        document.getElementById('imagBoton').style.display = 'block';
        document.getElementById('cerrarSec').style.display = 'block';
        document.getElementById('limpiar').style.display = 'block';
        document.getElementById("nombre").disabled = false;
        document.getElementById("precio").disabled = false;
        document.getElementById("descripcion").disabled = false;
        document.getElementById("nombreIma").disabled = false;
    }
}

function esribirInputs(){
    document.getElementById('nombre').value = Nombre;
    document.getElementById('precio').value = Precio;
    document.getElementById('descripcion').value = Descripcion;
    document.getElementById('imgPro').value = ImagenPro;
    document.getElementById('nombreIma').value = nomImgLocal;
    document.getElementById('estado').value = Estado;
}

async function actualizarDB(){
    LeerInputs();
    if(nomImgLocal != nomImgConsu){
        await subirImagen();
        await bajarImagen();
    }
    await update(ref(db,'Productos/'+ ID),{
        Nombre:Nombre,
        Precio:Precio,
        Descripcion:Descripcion,
        Estado:Estado,
        url:urlIma,
        NombreIma:nomImgLocal}).then(()=>{
            ImagenPro.src = urlIma;
            alert("se realizo actualizacion");
    }).catch((error)=>{
        alert("Surgio un error ")
    });
    Limpiar();
}


async function accionProducto(){
    if(camposLlenos() && NombreIma.value!=""){
        if(await subirImagen()){
            if(await bajarImagen()){
                inserDataBase();
            }
            else alert("Error intentar bajar la imagen.")
        }
        else alert("Error al intentar subir la imagen.");
    }
    else alert("Favor de ingresar bien los datos.");
}

function MostrarImage(){
    document.getElementById('EsIma').style.display = 'block';
    document.getElementById('agregar').style.display = 'none';
    document.getElementById('buscar').style.display = 'none';
    document.getElementById('modificar').style.display = 'none';
    document.getElementById('imagBoton').style.display = 'none';
    document.getElementById('cerrarSec').style.display = 'none';
    document.getElementById('limpiar').style.display = 'none';
}

function MostrarBuscar(){
    document.getElementById('agregar').style.display = 'none';
    document.getElementById('buscar').style.display = 'none';
    document.getElementById('buscarID').style.display = 'block';
    document.getElementById('modificar').style.display = 'none';
    document.getElementById('imagBoton').style.display = 'none';
    document.getElementById('cerrarSec').style.display = 'none';
    document.getElementById('limpiar').style.display = 'none';
    document.getElementById("IDNombre").disabled = false;
    document.getElementById("ID").disabled = false;
    document.getElementById("nombre").disabled = true;
    document.getElementById("precio").disabled = true;
    document.getElementById("descripcion").disabled = true;
    document.getElementById("nombreIma").disabled = true;
}

btnAgregar.addEventListener('click', accionProducto);
btnBuscarID.addEventListener('click', MostarDatosBase);
btnBuscar.addEventListener('click', MostrarBuscar);
btnModificar.addEventListener('click', actualizarDB);
btnLimpiar.addEventListener('click', Limpiar);
btnImagen.addEventListener('click', MostrarImage);
ImagenSu.addEventListener('change', cargarImagen);