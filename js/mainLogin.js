import { signInWithEmailAndPassword } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import {auth} 
from "./DbConec.js";

// Initialize Firebase

let btnLimpiar = document.getElementById('limpiar');
let validarCorreo = document.getElementById('ingresas');
let btnIngresas = document.getElementById('ingresas');

const LoForm = document.getElementById('loginform');

// Validaciones inputs

function BorrarTodo(){
    document.getElementById("correo").value = '';
    document.getElementById("contraseña").value = '';
}

LoForm.addEventListener('submit', async e =>{
    e.preventDefault();
    const Correo = LoForm['correo'].value;
    const Contra = LoForm['contraseña'].value;
    try{
        const InfoUsa = await signInWithEmailAndPassword(auth, Correo, Contra);
        location = '/html/Administrador.html';
    }catch(error){
        alert("La contrasena o el correo estan mal");
    }

});

btnLimpiar.addEventListener('click', BorrarTodo);
console.log('Estoy abajo');